package JDBCPractice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class PreparedUpdate {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedstatement = null;
		
		System.out.println("Enter Employee id and salary to be updated : ");
		Scanner scanner = new Scanner(System.in);
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		
		try {
			preparedstatement = connection.prepareStatement("update employee set salary = ? where empId = ?");
			preparedstatement.setInt(2, empId);
			preparedstatement.setDouble(1, salary);
			int result = preparedstatement.executeUpdate();
			
			if(result > 0){
				System.out.print("Employee data updated sucessfully...");
			}
			else{
				System.out.println("Record Not Found!!!!");
			}
			preparedstatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			try {
				if(connection != null){
				connection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

}
