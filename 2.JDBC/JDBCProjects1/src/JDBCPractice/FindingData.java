package JDBCPractice;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class FindingData {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ResultSetMetaData resultsetmetadata = null;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the employee id : ");
		int empId=scanner.nextInt();
		
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from employee where empId=" + empId);
			resultsetmetadata = resultSet.getMetaData();
			
			if(resultSet != null){
				while(resultSet.next()){
					System.out.print(resultsetmetadata.getColumnName(1)+ " : " + resultSet.getInt(1)    + " " + resultsetmetadata.getColumnName(2)+ " : " + resultSet.getString(2) + " ");
					System.out.print(resultsetmetadata.getColumnName(3)+ " : " + resultSet.getDouble(3) + " " + resultsetmetadata.getColumnName(4)+ " : " +  resultSet.getString(4) + " ");
					System.out.print(resultsetmetadata.getColumnName(5)+ " : " + resultSet.getString(5) + " " + resultsetmetadata.getColumnName(6)+ " : " +resultSet.getString(6) + " ");
					System.out.println();
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			if(connection != null){
				statement.close();
				connection.close();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
