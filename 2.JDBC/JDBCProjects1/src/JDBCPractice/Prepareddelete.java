package JDBCPractice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Prepareddelete {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedstatement = null;
		
		System.out.print("Enter employee id to be deleted : ");
		Scanner scanner = new Scanner(System.in);
		int empId = scanner.nextInt();
		
		String deleteQuery = "delete from employee where empId = ?";
		
		try {
			preparedstatement = connection.prepareStatement(deleteQuery);
			preparedstatement.setInt(1, empId);
			int result = preparedstatement.executeUpdate();
			
			if(result > 0){
				System.out.print("Employee Data Deleted Sucessfully....");
			}
			else{
				System.out.print("Deletion Failed!!!!!!!");
			}
			preparedstatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(connection != null){
			connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
