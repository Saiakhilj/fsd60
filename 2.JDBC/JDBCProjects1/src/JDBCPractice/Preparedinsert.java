package JDBCPractice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Preparedinsert {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedstatement = null;
		
		System.out.print("Enter the employee details : ");
		Scanner scanner = new Scanner(System.in);
		int empId = scanner.nextInt();
		String empName = scanner.next();
		double salary = scanner.nextDouble();
		String gender = scanner.next();
		String email = scanner.next();
		String password = scanner.next();
		
		String insertQuery = "insert into employee values(?,?,?,?,?,?)";
		
		try {
			preparedstatement = connection.prepareStatement(insertQuery);
			preparedstatement.setInt(1, empId);
			preparedstatement.setString(2, empName);
			preparedstatement.setDouble(3, salary);
			preparedstatement.setString(4, gender);
			preparedstatement.setString(5, email);
			preparedstatement.setString(6, password);
			
			int result = preparedstatement.executeUpdate();
			if(result > 0){
				System.out.println("Employee data inserted sucessfully.....");
			}
			else{
				System.out.println("Insertion Failed!!!!!!");
			}
			
			preparedstatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {				
				if (connection != null) {					
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}