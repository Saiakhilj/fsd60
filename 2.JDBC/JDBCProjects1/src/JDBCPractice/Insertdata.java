package JDBCPractice;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Insertdata {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		System.out.println("enter employee details : ");
		
		Scanner scanner = new Scanner(System.in);
		int empId = scanner.nextInt();
		String empName = scanner.next();
		double salary = scanner.nextDouble();
		String gender = scanner.next();
		String emailId = scanner.next();
		String password = scanner.next();
		
		String insertQuery = "insert into employee values (" + 
				empId + ", '" + empName + "', " + salary + ", '" + 
				gender + "', '" + emailId + "', '" + password + "')";
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
