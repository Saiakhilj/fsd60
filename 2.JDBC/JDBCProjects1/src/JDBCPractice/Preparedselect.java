package JDBCPractice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Preparedselect {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedstatement = null;
		ResultSet resultset = null;
		ResultSetMetaData resultsetmeta = null;
		
		System.out.println("Enter employee id to find : ");
		Scanner scanner = new Scanner(System.in);
		int empId = scanner.nextInt();
		
		String selectQuery = "select * from employee where empId = ?";
		System.out.println("Employee Details are : ");
		
		try {
			preparedstatement = connection.prepareStatement(selectQuery);
			preparedstatement.setInt(1, empId);
			resultsetmeta = preparedstatement.getMetaData();
			resultset = preparedstatement.executeQuery();
			
			if(resultset.next()){
				
					System.out.print(resultsetmeta.getColumnName(1) + " : " + resultset.getInt(1)    + " " + resultsetmeta.getColumnName(2) + " : " + resultset.getString(2) + " ");
					System.out.print(resultsetmeta.getColumnName(3) + " : " + resultset.getDouble(3) + " " + resultsetmeta.getColumnName(4) + " : " + resultset.getString(4) + " ");
					System.out.print(resultsetmeta.getColumnName(5) + " : " + resultset.getString(5) + " " + resultsetmeta.getColumnName(6) + " : " + resultset.getString(6) + " ");
					System.out.println();
				
			}
			else{
				System.out.print("Record Not Found!!!");
			}
			
			preparedstatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(connection != null){
			connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
