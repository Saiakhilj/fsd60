
package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Insert an Employee Record
public class Demo03 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId to be updated : ");
		
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		
		
		String insertQuery = "update employee set salary = ? where empId = ?";
		
		try {
			preparedStatement = connection.prepareStatement(insertQuery);
						
			preparedStatement.setInt(2, empId);
			preparedStatement.setDouble(1, salary);
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Updated Sucessfully...");
			} else {
				System.out.println("Record Updation Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}