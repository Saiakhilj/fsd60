
package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Insert an Employee Record
public class Demo04 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId to be Deleted : ");
		
		int empId = scanner.nextInt();
		
		String insertQuery = "delete from employee where empId = ?";
		
		try {
			preparedStatement = connection.prepareStatement(insertQuery);
						
			preparedStatement.setInt(1, empId);
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Deleted Sucessfully...");
			} else {
				System.out.println("Record Deletion Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}