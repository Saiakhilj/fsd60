package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo05 {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedstatement = null;
		ResultSet resultSet = null;
		
		System.out.println("Enter Employee ID to Find : ");
		Scanner scanner = new Scanner(System.in);
		int empId = scanner.nextInt();
		
		try {
			preparedstatement = connection.prepareStatement("select * from employee where empId = ?" );
			preparedstatement.setInt(1, empId);
			resultSet = preparedstatement.executeQuery();
			
			if(resultSet.next()){
				
					
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					
				
			}
			else{
				System.out.print("Record not found!!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
