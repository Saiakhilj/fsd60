package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetEmployeeById")
public class GetEmployeeById extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("text/html");

		int empId = Integer.parseInt(request.getParameter("empId"));

		EmployeeDao employeeDao = new EmployeeDao();
		Employee employee = employeeDao.getEmployeeById(empId);

		out.print("<body bgcolor='lightyellow' text='green'> <center>");	
		if(employee != null){
			request.setAttribute("employee", employee);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("getEmployeeById.jsp");
			requestDispatcher.forward(request, response);
			
		}
		else{
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
			requestDispatcher.include(request, response);
			out.print("<h1 style='colour:red;'>");
			out.print("Employee Details Not Found!!!");
			out.print("</h1>");
		}
		out.print("</center></body>");

		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
