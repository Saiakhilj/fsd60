package com.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.AccessoriesDAO;
import com.dto.Accessories;

@WebServlet("/EditAccessoriesServlet")
public class EditAccessoriesServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int prodId = Integer.parseInt(request.getParameter("prodId"));
		
		AccessoriesDAO accessoriesDAO = new AccessoriesDAO();
		Accessories accessories = accessoriesDAO.getAccessorie(prodId);
		
		request.setAttribute("accessories", accessories);
		RequestDispatcher rd = request.getRequestDispatcher("EditAccessoriesAdmin.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
