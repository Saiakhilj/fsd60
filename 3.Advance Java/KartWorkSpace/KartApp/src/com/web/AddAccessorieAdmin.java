package com.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.AccessoriesDAO;
import com.dto.Accessories;

@WebServlet("/AddAccessorieAdmin")
public class AddAccessorieAdmin extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String prodName = request.getParameter("prodName");
		String prodType = request.getParameter("prodType");
		String manufacturer = request.getParameter("manufacturer");
		double prodPrice = Double.parseDouble(request.getParameter("prodPrice"));
		
		Accessories accessorie = new Accessories();
		accessorie.setProdName(prodName);
		accessorie.setProdType(prodType);
		accessorie.setManufacturer(manufacturer);
		accessorie.setProdPrice(prodPrice);
		
		AccessoriesDAO accessorieDAO = new AccessoriesDAO();
		int result = accessorieDAO.addAccessorie(accessorie);
		
		if(result > 0) {
			int prodId = accessorieDAO.getLatestProductId();			
			
			request.setAttribute("prodId", prodId);
			RequestDispatcher rd = request.getRequestDispatcher("ProductAdded.jsp");
			rd.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
