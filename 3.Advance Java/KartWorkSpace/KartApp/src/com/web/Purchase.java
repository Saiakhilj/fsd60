package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.KartDAO;

@WebServlet("/Purchase")
public class Purchase extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String loginId = (String) session.getAttribute("loginId");
		
		KartDAO kartDAO = new KartDAO();
		int result = kartDAO.deleteItems(loginId);
		
		if(result > 0 ) {
			RequestDispatcher rd = request.getRequestDispatcher("DonePurchase.jsp");
			rd.forward(request, response);
		} else {						
			RequestDispatcher rd = request.getRequestDispatcher("NoItems.jsp");
			rd.forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
