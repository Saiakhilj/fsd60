package com.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.KartDAO;
import com.dto.Kart;

@WebServlet("/DisplayKart")
public class DisplayKart extends HttpServlet {	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();	
		String loginId = (String) session.getAttribute("loginId");
		
		KartDAO kartDAO = new KartDAO();
		List<Kart> kartList = kartDAO.getKart(loginId);
		
		if(kartList != null) {
			request.setAttribute("kartList", kartList);
			RequestDispatcher rd = request.getRequestDispatcher("DisplayKart.jsp");
			rd.forward(request, response);
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("DisplayKart2.jsp");
			rd.forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
