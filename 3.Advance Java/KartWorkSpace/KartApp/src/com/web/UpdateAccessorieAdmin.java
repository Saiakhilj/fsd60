package com.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.AccessoriesDAO;
import com.dto.Accessories;

@WebServlet("/UpdateAccessorieAdmin")
public class UpdateAccessorieAdmin extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int prodId = Integer.parseInt(request.getParameter("prodId"));
		String prodName = request.getParameter("prodName");
		String prodType = request.getParameter("prodType");
		String manufacturer = request.getParameter("manufacturer");
		double prodPrice = Double.parseDouble(request.getParameter("prodPrice"));
		
		Accessories accessorie = new Accessories(prodId, prodName, prodType, manufacturer, prodPrice);
				
		AccessoriesDAO accessorieDAO = new AccessoriesDAO();
		int result = accessorieDAO.updateAccessorie(accessorie);
		
		if(result > 0) {
			RequestDispatcher rd = request.getRequestDispatcher("DisplayAccessoriesAdmin");
			rd.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
