package com.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.AccessoriesDAO;

@WebServlet("/DeleteAccessoriesServlet")
public class DeleteAccessoriesServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int prodId = Integer.parseInt(request.getParameter("prodId"));
		
		AccessoriesDAO accessoriesDAO = new AccessoriesDAO();
		int result = accessoriesDAO.deleteAccessorie(prodId);
		
		if(result > 0) {
			RequestDispatcher rd = request.getRequestDispatcher("DisplayAccessoriesAdmin");
			rd.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
