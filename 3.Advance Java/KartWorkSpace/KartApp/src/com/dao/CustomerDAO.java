package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Customer;

public class CustomerDAO {

	public Customer getCustomer(String loginId, String password) {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String SELECT = "Select * from Customer where loginId=? and password=?";
		
		con = DbConnection.getConnection();
		
		try {
			pst = con.prepareStatement(SELECT);
			pst.setString(1, loginId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if(rs.next()) {
				Customer customer = new Customer();
				
				customer.setCustId(rs.getInt(1));
				customer.setCustName(rs.getString(2));
				customer.setMobile(rs.getString(3));
				customer.setEmailId(rs.getString(4));
				customer.setLoginId(rs.getString(5));
				customer.setPassword(rs.getString(6));
				customer.setStatus(rs.getString(7));
				return customer;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public int registerCustomer(Customer customer) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		
		String INSERT = "Insert into Customer (custName, mobile, emailId, loginId, password, status) values (?,?,?,?,?,?)";		
		con = DbConnection.getConnection();
		
		try {
			pst = con.prepareStatement(INSERT);	
			
			pst.setString(1, customer.getCustName());
			pst.setString(2, customer.getMobile());
			pst.setString(3, customer.getEmailId());
			pst.setString(4, customer.getLoginId());
			pst.setString(5, customer.getPassword());	
			pst.setString(6, customer.getStatus());
			
			result = pst.executeUpdate();			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

	public List<Customer> getCustomers() {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String SELECT = "Select * from Customer";
		
		con = DbConnection.getConnection();
		
		try {
			pst = con.prepareStatement(SELECT);;
			rs = pst.executeQuery();
			
			List<Customer> customerList = new ArrayList<Customer>();
			
			while(rs.next()) {
				Customer customer = new Customer();
				
				customer.setCustId(rs.getInt(1));
				customer.setCustName(rs.getString(2));
				customer.setMobile(rs.getString(3));
				customer.setEmailId(rs.getString(4));
				customer.setLoginId(rs.getString(5));
				customer.setPassword(rs.getString(6));
				customer.setStatus(rs.getString(7));
				
				customerList.add(customer);
			}
			
			return customerList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public int customerStatusUpdate(int custId, String status) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		
		String UPDATE = "Update Customer set status=? where custId=?";		
		con = DbConnection.getConnection();
		
		try {
			pst = con.prepareStatement(UPDATE);
			
			pst.setInt(2, custId);
			pst.setString(1, status);

			result = pst.executeUpdate();			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

	public int getLatestCustomerId() {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String SELECT = "Select max(custId) from Customer";
		
		con = DbConnection.getConnection();
		
		try {
			pst = con.prepareStatement(SELECT);
			rs = pst.executeQuery();
			
			if(rs.next()) {				
				return rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

}
