package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.db.DbConnection;
import com.dto.Kart;

public class OrdersDAO {

	
	public int orderItems(Kart kart) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		
		con = DbConnection.getConnection();
		String INSERT = "insert into Orders values (?,?,?,?,?,?)";
		
		try {
			pst=con.prepareStatement(INSERT);
			
			pst.setInt(1, kart.getProdId());
			pst.setString(2, kart.getProdName());
			pst.setString(3, kart.getProdType());
			pst.setString(4, kart.getManufacturer());
			pst.setDouble(5, kart.getProdPrice());
			pst.setString(6, kart.getLoginId());
			
			result = pst.executeUpdate();
			return result;
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
}
