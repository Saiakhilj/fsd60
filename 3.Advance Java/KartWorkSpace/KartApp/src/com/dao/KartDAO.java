package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Accessories;
import com.dto.Kart;

public class KartDAO {

	

	public int addAccessorie(Accessories accessorie, String loginId) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
			
		con = DbConnection.getConnection();
		String INSERT = "insert into Kart values (?,?,?,?,?,?)";
		
		try {
			pst=con.prepareStatement(INSERT);
			
			pst.setInt(1, accessorie.getProdId());
			pst.setString(2, accessorie.getProdName());
			pst.setString(3, accessorie.getProdType());
			pst.setString(4, accessorie.getManufacturer());
			pst.setDouble(5, accessorie.getProdPrice());
			pst.setString(6, loginId);
			
			result = pst.executeUpdate();
			return result;
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
				
		return 0;
	}

	public List<Kart> getKart(String loginId) {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int count = 0;
		
		String SELECT = "Select * from Kart where loginId=?";
		
		con = DbConnection.getConnection();
		
		try {
			pst = con.prepareStatement(SELECT);
			pst.setString(1, loginId);
			rs = pst.executeQuery();
			
			List<Kart> kartList = new ArrayList<Kart>();
			
			while(rs.next()) {
				count++;
				Kart kart = new Kart();
				
				kart.setProdId(rs.getInt(1));
				kart.setProdName(rs.getString(2));
				kart.setProdType(rs.getString(3));
				kart.setManufacturer(rs.getString(4));
				kart.setProdPrice(rs.getDouble(5));
				kart.setLoginId(rs.getString(6));			
				
				kartList.add(kart);
			}
			
			if(count == 0) {
				return null;
			}
			
			return kartList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public int deleteItem(int prodId) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
			
		con = DbConnection.getConnection();
		String DELETE = "Delete from Kart where prodId=?";
		
		try {
			pst=con.prepareStatement(DELETE);		
			pst.setInt(1, prodId);
			result = pst.executeUpdate();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		

		return 0;
	}

	public int deleteItems(String loginId) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
			
		con = DbConnection.getConnection();
		String DELETE = "Delete from Kart where loginId=?";
		
		try {
			pst=con.prepareStatement(DELETE);		
			pst.setString(1, loginId);
			result = pst.executeUpdate();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public List<Kart> getKartItems() {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int count = 0;
String SELECT = "Select * from Kart";
		
		con = DbConnection.getConnection();
		
		try {
			pst = con.prepareStatement(SELECT);
			
			rs = pst.executeQuery();
			
			List<Kart> kartList = new ArrayList<Kart>();
			
			while(rs.next()) {
				count++;
				Kart kart = new Kart();
				
				kart.setProdId(rs.getInt(1));
				kart.setProdName(rs.getString(2));
				kart.setProdType(rs.getString(3));
				kart.setManufacturer(rs.getString(4));
				kart.setProdPrice(rs.getDouble(5));
				kart.setLoginId(rs.getString(6));			
				
				kartList.add(kart);
			}
			
			if(count == 0) {
				return null;
			}
			
			return kartList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
