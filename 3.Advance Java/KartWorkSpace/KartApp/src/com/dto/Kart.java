package com.dto;

public class Kart {
	private int prodId;
	private String prodName;
	private String prodType;
	private String manufacturer;
	private double prodPrice;
	private String loginId;
	
	public Kart() {
	}

	public Kart(int prodId, String prodName, String prodType, String manufacturer, double prodPrice, String loginId) {
		
		this.prodId = prodId;
		this.prodName = prodName;
		this.prodType = prodType;
		this.manufacturer = manufacturer;
		this.prodPrice = prodPrice;
		this.loginId = loginId;
	}

	public int getProdId() {
		return prodId;
	}

	public void setProdId(int prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public double getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(double prodPrice) {
		this.prodPrice = prodPrice;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	@Override
	public String toString() {
		return "Kart [prodId=" + prodId + ", prodName=" + prodName + ", prodType=" + prodType + ", manufacturer="
				+ manufacturer + ", prodPrice=" + prodPrice + ", loginId=" + loginId + "]";
	}
		
}
