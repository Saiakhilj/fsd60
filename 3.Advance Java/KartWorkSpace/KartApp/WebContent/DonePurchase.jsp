<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="5;url=CustomerHomePage.jsp" />
<script type="text/css">
.links{

   position:fixed;
   right:10px;
   top:5px;
}
</script>
<title>Thank You</title>
</head>
<body bgcolor="lightgreen" text="blue">

<form align="right">
	<label class="links">		
		<a href="CustomerHomePage.jsp">Home</a> &nbsp; &nbsp;
		<a href="Login.html">Logout</a>
  </label>
</form>

<center>
	<h2>Thanks For Purchasing in Our WebSite</h2>
	<h3>Click <a href="CustomerHomePage.jsp">here</a> to continue shopping</h3>
</center>
</body>
</html>