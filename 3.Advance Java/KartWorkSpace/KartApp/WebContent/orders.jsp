<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>orders</title>
</head>
<body>
<jsp:include page="AdminHomePage.jsp" />

	<center>
		<table border="2">
			<tr>
				<th>Product ID</th>
				<th>Product Name</th>
				<th>Product Type</th>
				<th>Manufacturer</th>
				<th>Product Price</th>
				<th>Customer Id</th>
				
			</tr>
			
			<c:forEach var="kart" items="${kartList}">
				<tr>
					<td>${kart.prodId}</td>
					<td>${kart.prodName}</td>
					<td>${kart.prodType}</td>
					<td>${kart.manufacturer}</td>
					<td>${kart.prodPrice}</td>
					<td>${kart.loginId}</td>
					
				</tr>
			</c:forEach>
			
		</table>	
	</center>
</body>
</html>